(Français) 
> Prizm n'est malheureusement plus en mesure de poursuivre son développement et de maintenir ses produits.  
> Pour ces raisons, les serveurs ont été coupés, ne permettant plus les mises à jour automatiques.     
> L'équipe Prizm a développé une version autonome qui modifie le produit pour qu'il puisse continuer de fonctionner.  
> Cette mise à jour a été rendu disponible le 13 Août 2018. Si votre Prizm ne s'est pas mis à jour automatiquement, veuillez suivre la procédure ci-dessous pour appliquer la mise à jour manuellement.
> Une fois celle-ci faite, vous devrez créer un nouveau profil. Vous pourrez retrouver vos stations en les important grâce au fichier qui vous a été envoyé par email.     
> Si vous devez refaire cette procédure par la suite, vous pourrez récupérer toutes vos stations automatiquement en créant un nouveau profil.

(English) 
> Prizm discontinued operations and is no longer in a position to keep everything up and running.   
> For these reasons, Prizm’s servers have shut down, eliminating the possibility of automatic and server based updates.  
> To mitigate these issues, the Prizm Team developed an autonomous software update that will change the device so that it will be able to continue to operate as a standalone device.   
> This update was deployed on August 13, 2018. If your Prizm did not receive this update, please follow the instructions below to manually apply it. 
> Once updated, you will have to create a new profile. You will be able to retrieve your stations by importing the file sent by email.    
> If you have to follow this procedure again, you will be able to retrieve your stations automatically when creating a new profile.

# Mettre à jour Prizm manuellement
(_Version française - see below for the [**english version**](#update-prizm-manually)_)  


#### A. Créer le réseau WiFi Prizm :

1. Débranchez votre Prizm s'il était branché
2. Maintenez le bouton _Play_ appuyé (bouton en bas de la face centrale), puis rebrancher votre Prizm. Continuez de maintenir le bouton _Play_ jusqu'à ce que la LED devienne jaune (cela prend au moins dix secondes)
3. Prizm aura alors créé un réseau WiFi appelé soit "Prizm-Recovery", soit "Prizm-Recovery-XXXXXX"

_Si le réseau WiFi créé s'appelle "Prizm-Recovery-XXXXXX", vous n'avez pas à faire l'étape suivante B et vous pouvez passer directement à l'étape C_


#### B. Se connecter au réseau "Prizm-Recovery" :
4. Téléchargez sur votre ordinateur le fichier de mise à jour "Prizm_Preliminary-Update.swu"
5. Connectez votre ordinateur au réseau WiFi "Prizm-Recovery" 
6. Tapez ensuite cette adresse dans votre navigateur web (Chrome, Firefox, Safari...) : http://192.168.0.1
7. Cliquez sur le bouton "Choose file"
8. Choisissez le fichier "Prizm_Preliminary-Update.swu" précédemment téléchargé
9. La procédure dure environ 15min. **Attention à ne pas débrancher Prizm lorsque la mise à jour est en cours**
10. Quand elle est terminée, Prizm redémarrera tout seul (la LED ne sera plus jaune mais blanche puis verte)
11. Vous allez maintenant devoir installer une deuxième mise à jour manuelle : répétez l'étape A pour que Prizm créé à nouveau son réseau WiFi, qui s'appelle désormais "Prizm-Recovery-XXXXXX", et passez à l'étape suivante


#### C. Se connecter au réseau "Prizm-Recovery-XXXXXX" :
4. Téléchargez sur votre ordinateur le fichier de mise à jour "Prizm_Update-V20.7.swu"
5. Connectez votre ordinateur au réseau WiFi "Prizm-Recovery-XXXXXX" 
6. Tapez ensuite cette adresse dans votre navigateur web (Chrome, Firefox, Safari...) : http://192.168.0.1
7. Cliquez sur le bouton "Choose file"
8. Choisissez le fichier "Prizm_Update-V20.swu" précédemment téléchargé
9. La procédure dure environ 15min. **Attention à ne pas débrancher Prizm lorsque la mise à jour est en cours**
10. Quand elle est terminée, Prizm redémarrera tout seul (la LED ne sera plus jaune mais blanche puis verte)
11. Débranchez puis rebrancher votre Prizm lorsque la LED est verte
12. Votre Prizm est maintenant à jour, vous pouvez le configurer sur le WiFi de votre maison via l'application

---
# Update Prizm manually  
_(English version)_  

#### A. Create the Prizm WiFi network :

1. Unplug your Prizm if it was plugged in
2. Press on _Play_ button (at the bottom of the front face), then plug your Prizm back in while keeping the button _Play_ pushed until the LED light turns yellow (you'll have to wait at least 10 seconds)
3. Prizm has created a WiFi network called "Prizm recovery" or "Prizm-Recovery-XXXXXX"

_If the WiFi network created is "Prizm-Recovery-XXXXXX", you don't have to do the next step B and can jump directly to the step C_


#### B. Connect your computer to Prizm WiFi network "Prizm recovery":
4. Download on your computer the update file "Prizm_Preliminary-Update.swu"
5. Connect your computer to Prizm WiFi network "Prizm recovery"
6. Write down in the search bar of your web browser (Chrome, Firefox, Safari...): http://192.168.0.1
7. Click on "Choose file"
8. Choose the file "Prizm_Preliminary-Update.swu" you previously downloaded.
9. The update lasts about 15 minutes. **Please do not unplug Prizm while the update is being done**
10. When the update is over, Prizm will restart by itself (the LED will no longer be yellow, but white then will turn to green)
11. You will now have to do a second manual update: repeat the step A to make you Prizm create its WiFi network again, which is now called ""Prizm-Recovery-XXXXXX""


#### C. Connect your computer to Prizm WiFi network "Prizm-Recovery-XXXXXX":
4. Download on your computer the update file "Prizm_Update-V20.7.swu"
5. Connect your computer to Prizm WiFi network "Prizm recovery-XXXXXX"
6. Write down in the search bar of your web browser (Chrome, Firefox, Safari...): http://192.168.0.1
7. Click on "Choose file"
8. Choose the file "Prizm_Update-V20.swu" you previously downloaded.
9. The update lasts about 15 minutes. **Please do not unplug Prizm while the update is being done**
10. When the update is over, Prizm will restart by itself (the LED will no longer be yellow, but white then will turn to green)
11. When your Led is green, unplug and plug your Prizm back in
12. Prizm is now updated, you can now connect it to your Home WiFi with the app


